# Stock Trading Platform Project

## Overview

The aim of this project is to create a stock trading platform which will perform the following core features:

1. A web UI should facilitate users to:
    * Request to make a Trade on a given Stock
    * Browse their trading history
    * View the status of each of their historical Trades

2. For the initial version there will be no authentication and a single user is assumed, i.e. there is no requirement to manage users in the initial revision.

3. You should use MySQL for any persistent storage.

4. You should aim to complete the core functionality described above before attempting any of the extensions described later in the document. The core technical elements should be designed by you. This document is provided as an outline, however all data storage, REST APIs etc. should be designed by your team.

### System Architecture
It is up to you and your team to decide on an overall architecture for the system. However, you should consider enterprise deployment environments and aim for a microservice style architecture.

The following architecture is the suggested architecture for the core components:

![Trade Project Diagram](https://www.benivade.com/neueda-training/Tech2020/CoreTradeProject2020.png)

1. Trade REST API: This should be written in Java as a spring boot REST application. It will accept HTTP REST requests to Create & Retrieve Trade records from the TradeDB. Update and Delete of Trade records should be subject to sensible business considerations.

2. TradeDB: This should be data stored in a MySQL database. You do not need to deploy a production database (e.g. you could use a containerized MySQL database).

3. Dummy Trade Fullfillment Service: Source code for this component is provided at https://bitbucket.org/fcallaly/dummy-trade-filler/src/master/ -
grab your own copy of this code and take a look. The code talks to a MongoDB database so you'll need to adjust it to talk to MySQL instead. You'll also need to make sure the entity classes are consistent with your MySQL schema. This service will read Trade records from the "TradeDB" and simulate fullfilling or rejecting those Trades, it will mark the corresponding Trade record with an appropriate status e.g. PROCESSING, FULLFILLED, REJECTED.

4. Web Frontend: This is a web UI written in Javascript, Angular, or any other front-end framework you are experienced with (check with your instructor first). This UI should allow users to browse their Trading history, and request for Trades to be made by sending HTTP requests to the Trade REST API.

### Optional Extensions

Once you have finished the core features, you may add some of the optional extras described below, or come up with your own extensions that you think would suit this system. Check with your instructors before beginning implementation of any extension features.

Some extra optional features you may add include:

    * A service that manages the users current portfolio, e.g. what Stocks, Cash, Bonds etc. they are currently holding.
    OR
    * A service that will give advice on suggested trades e.g. for a given stock should the user BUY, SELL or HOLD
    OR
    * A service that will automatically make trades based on user defined criteria e.g. the stock to trade, the maximum value of trades, when to stop making trades
    OR
    * A service that will integrate with Slack to give users updates on the status of trades

An example of how some of these components might fit within the system is shown below. However, these are SUGGESTIONS, the design of these elements should be driven by your team. You are definitely NOT expected to complete all of the elements shown below.

![Trade Project Diagram](https://www.benivade.com/neueda-training/Tech2020/ExtendedTradeProject2020.png)


5. Portfolio REST API: This could be written in Java and may even be part of the same spring boot application as the "Trade REST API". Alternatively, it may be a standalone spring boot application. This service should keep track of Cash, Stocks etc. currently held.

6. PortfolioDB: Similar to TradeDB this should be data stored in a MySQL database.

7. Live Price Service REST API: If included, this may be written in Python or Java. This component would make price data available to your other components through a REST API. E.g. so that live price data could be shown in the UI. This component may read it's price data from a live service such as Yahoo Finance or elsewhere.

8. External Price Service: This is an External service e.g. Yahoo Finance.

9. Trade Advice REST API: If included, this may be written in Python OR Java. This service would receive RESTful requests for advice regarding stock tickers. It should return an answer indicating whether the advice is to BUY, SELL or HOLD that stock. E.g. The UI may use this service to indicate to the user what may be a sensible Trading option.

### Teamwork
It is expected that you work closely as a team during this project.

Your team should be self-organising, but should raise issues with instructors if they are potential blockers to progress. 

Your team should keep track of all source code with git and bitbucket.

You may choose to create a separate bitbucket repository for each component that you tackle e.g. front-end code can be in its own repository. If you create more than one spring-boot application, then each can have its own bitbucket repository. To keep track of your repositories, you can use a single bitbucket 'Project' that each of your repositories is part of.

Your instructor and team members need to access all repositories, so they should be either A) made public OR B) shared with your instructor and all team members.

Throughout your work, you should ensure good communication and organise regular check-ins with each other.



### Appendix A: Optional Extra - Portfolio REST API

This API would be used to keep track of a personal portfolio of cash and investments (e.g. stocks, bonds).

The API would facilitate CRUD operations for this data, however you may choose to only implement some of the standard CRUD operations given the available time.

Some example operations that would be facilitated by this API:
- Add some stocks for a particular ticker to your portfolio
- Add some cash to your portfolio.
- Remove some cash from your portfolio


### Appendix B: UI Ideas

The screen below might give you some ideas about the type of UI that could be useful. You are NOT expected to implement the screen below exactly as it is shown. This is JUST FOR DEMONSTRATION of the type of thing that COULD be shown.

![Demonstration Portfolio UI](https://www.benivade.com/neueda-training/Tech2020/DemoPortfolioScreen.png)